# -*- coding: utf-8 -*-
from beancount.ingest import extract
from beancount_degiro import DegiroAccount, DegiroDE

# example importer config for Degiro importer
# use with "bean-extract ConfigDegiro.py /path/to/Account.csv
# Account.csv is the "Account" activity report from Degiro

account = DegiroAccount(
    language = DegiroDE, # defines descriptors for transaction descriptions
                         # Feel free to add your favourite language to degiro_lang.py

    currency = 'EUR',    # main currency
																			# Available tokens:
    LiquidityAccount       = 'Assets:Invest:Degiro:{currency}',				# {currency}
    StocksAccount          = 'Assets:Invest:Stock:Degiro:{ticker}',			# {isin}, {ticker}
    # You probably want to use the same account for stocks and splits, otherwise the inventory will be messed up
    SplitsAccount          = 'Assets:Invest:StockSplits:Degiro:{ticker}',	# {isin}, {ticker}
    FeesAccount            = 'Expenses:Invest:Fees:Degiro:{currency}',		# {currency}
    InterestAccount        = 'Expenses:Invest:Interests:Degiro',			# {currency}
    PnLAccount             = 'Income:Invest:PnL:Degiro',					# {isin}, {ticker}, {currency}
    DivIncomeAccount       = 'Income:Invest:Dividends:Degiro',				# {isin}, {ticker}, {currency}
    WhtAccount             = 'Expenses:Invest:Taxes:Degiro',				# {isin}, {ticker}, {currency}
    RoundingErrorAccount   = 'Expenses:Invest:Fees:RoundingErrors',			# {currency}

    # DepositAccount: put in your checkings account if you want deposit transactions
    #DepositAccount         = 'Assets:Bank:Savings'							# {currency}
    # ticker cache speeds up automatic ISIN -> ticker mapping
    TickerCacheFile        = '.ticker_cache'
)

CONFIG = [account]
extract.HEADER = '' # remove unnesseccary terminal output

