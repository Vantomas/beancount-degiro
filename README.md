# Beancount Degiro importer

`beancount-degiro` provides an importer for converting CSV exports of
[Degiro] account to the [Beancount] format.

## Installation

```sh
$ pip install beancount-degiro
```

## Usage
Look at example importer config ConfigDegiro.py, tune it and run "bean-extract ConfigDegiro.py /path/to/Account.csv"

Account.csv is the "Account" activity report from Degiro.


[Beancount]: http://furius.ca/beancount/
[Degiro]: https://www.degiro.de/
